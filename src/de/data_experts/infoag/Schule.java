package de.data_experts.infoag;

import java.util.ArrayList;
import java.util.List;

public class Schule {

    public static void main(String[] args) {
        List<String> zustaende = new ArrayList<String>();

        zustaende.add("K");
        zustaende.add("S");
        zustaende.add("O");

        for (int anzahlTage = 1; anzahlTage < 31; anzahlTage++) {

            List<String> moeglicheKombis = new ArrayList<String>();
            for (int i = 0; i < anzahlTage; i++) {
                if (moeglicheKombis.isEmpty()) {
                    for (int j = 0; j < zustaende.size(); j++) {
                        moeglicheKombis.add(zustaende.get(j));
                    }
                } else {
                    List<String> neueKombinationen = new ArrayList<String>();
                    for (String bisherigeKombi : moeglicheKombis) {
                        for (String zustand : zustaende) {
                            neueKombinationen.add(bisherigeKombi + zustand);
                        }
                    }

                    moeglicheKombis = neueKombinationen;
                }
            }

            List<String> guenstigeKombis = new ArrayList<String>();
            for (String kombi : moeglicheKombis) {
                if (kombi.contains("K")) {
                    continue;
                }
                if (kombi.contains("SSS")) {
                    continue;
                }

                guenstigeKombis.add(kombi);
            }

            float p = (float) guenstigeKombis.size() / (float) moeglicheKombis.size();

            System.out.println("Wahrscheinlichkeit f�r " + anzahlTage + " Tage: " + (p * 100) + "%     :-)");
        }

    }
}
