package de.data_experts.infoag;


public class MainGame {
	
	
	public final int WIDTH = 480;
	public final int HEIGHT = 480;
	public final String GAMENAME = "Freaky Jump";
	
	private gameFrame frame;
	
	public static void main(String[] args){
		MainGame game = new MainGame();
		game.run();
	}
	
	public void run(){
		init();
		while(true){
			update();
			
		}
	}
	
	public void init(){
		frame = new gameFrame(GAMENAME, WIDTH, HEIGHT);
	}
	
	public void update(){
		redraw(); //last action in this method
	}
	
	public void redraw(){
		frame.draw();
	}
	
}
