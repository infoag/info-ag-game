package de.data_experts.infoag;

import java.util.Random;

public class Wuerfeln {

	public static void main(String[] args) {
		
		Random richard = new Random(); // Generator f�r Zufallszahlen anlegen

		int gewinnePaul = 0;
		int gewinnePeter = 0;
		
		for (int spielNr=0; spielNr<95555555;spielNr++) {
		// 1 Spiel
		int paulsSumme = 0;
		int petersSumme = 0;

		// Paul w�rfelt
		for (int i = 0; i < 6; i++) {
			int paulsWurf = richard.nextInt(6) + 1;
			paulsSumme = paulsSumme + paulsWurf;
		}

		
		// Peter w�rfelt
		for (int i = 0; i < 6; i++) {
			int petersWurf = richard.nextInt(6) + 1;
			petersSumme = petersSumme + petersWurf;
		}
		
		
		

		
		if (paulsSumme > petersSumme) {
			// Paul hat gewonnen
			gewinnePaul++;
		} else if (petersSumme > paulsSumme) {
			// Peter gewonnen
			gewinnePeter++;
		} else {
			// Gleichstand
		}
		} // Ende des Spiels
		
		System.out.println("Paul: " + gewinnePaul);
		System.out.println("Peter: " + gewinnePeter);
		
		double petersWahrscheinlichkeit = ((double) gewinnePeter)/( (double) gewinnePaul + gewinnePeter);
		System.out.println(petersWahrscheinlichkeit);

	}

}
