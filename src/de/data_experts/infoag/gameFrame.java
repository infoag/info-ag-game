package de.data_experts.infoag;

import javax.swing.JFrame;

public class gameFrame extends JFrame{

	private static final long serialVersionUID = 1L;

	public gameFrame(String TITLE,int WIDTH,int HEIGHT) {
		super(TITLE);
		this.setSize(WIDTH, HEIGHT);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void draw(){
		
	}
}
